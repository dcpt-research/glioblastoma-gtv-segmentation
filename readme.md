# Glioblastoma GTV segmentation
Welcome to the repository for our deep learning models developed for glioblastoma (GBM) segmentation, specifically targeting the Gross Tumor Volume (GTV) in post-operative radiotherapy (RT). For more details see: DOI [10.1016/j.phro.2024.100620](https://www.phiro.science/article/S2405-6316(24)00090-3/fulltext)

## Overview
This repository contains two deep learning models:

- Edited-GTV Model: Trained on GTVs that have been edited by a single radiation oncologist, using modalities T1C, T1, FLAIR and CT.
- Edited-GTV Model: Same training data, but using only using T1C.

Both models were trained using the [no-new-UNet (nnUNet) framework](https://github.com/MIC-DKFZ/nnUNet). nnUNet is a plug and play framework that can automatically configure of network depth and hyper-parameters.

## Data
The models are trained on retrospective data from GBM patients treated at Aarhus University Hospital between 2012 and 2019. This dataset includes:

- Planning CT and MRI scans (T1, T1C, and FLAIR)
- Clinical GTV delineations reviewed and used for actual treatments
- Edited GTV delineations revised by a single radiation oncologist to conform optimally with the information in the imaging.


## Usage
The models should be used for research purposes only. This repository includes the model weights to make predictions using the nnUNet framework. The models can be downloaded from the following [google drive](https://drive.google.com/drive/folders/1d2nQUT8qBThOrHmonOTkKq5qDopM_ONp?usp=sharing).
To use the models from the paper, [nnUNet version 1.7](https://github.com/MIC-DKFZ/nnUNet/tree/nnunetv1) is needed. In the image below is a folder structure similar to what is suggested by the nnUNet guidelines. Placing the folders from the google drive in the 3d_fullres folder should make nnUNet recognize the models.

<p align="center">
  <img src="nnUNet_folder_structure.png" />
</p>

Below is an example snippet of how to run predictions on a folder, using the single modality model. Note that if you have a setup with multiple GPUs, you can use CUDA_VISIBLE_DEVICES=0 to control which of them the prediction should be run on.

```bash
CUDA_VISIBLE_DEVICES=0 nnUNet_predict -i YourDrive/nnUNet_raw_data_base/nnUNet_raw_data/GBM_image_folder -o YourDrive/nnUNet_inference/GBM_GTV_prediction_folder -t 600 -m 3d_fullres
```

To use the multi-modal model, the image modalities should be coded as follows in their file name:

Multi-modal model: 
- 0000: T1C
- 0001: FLAIR
- 0002: CT
- 0003: T1





## Contributions
We welcome contributions from the research community. Whether it’s enhancing the model, refining the data processing pipeline, or proposing new evaluation metrics, your input is valuable.

## Contact
For any questions or further information, please reach out to: Jesper F. Kallehauge, jespkall@rm.dk

We hope this repository serves as a valuable resource for advancing the field of automated GTV segmentation in glioblastoma treatment.